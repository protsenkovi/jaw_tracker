
import numpy as np

class PinTracker:
    def __init__(self):
        self.track_circles = []
        self.track_circles_idxs = []

    def append_green_dot_position(self, frame_idx, circle):
        if circle is None:
            if len(self.track_circles) > 0:
                self.track_circles_idxs.append(frame_idx)
                self.track_circles.append(circle)
        else:
            self.track_circles_idxs.append(frame_idx)
            self.track_circles.append(circle)

    def get_track_without_outliers(self):
        if len(self.track_circles) == 0:
            return np.empty(0)

        new_track_circles = [self.track_circles[0]]
        last_seen_circle = self.track_circles[0]
        for circle2 in self.track_circles[1:]:
            if not circle2 is None:
                new_track_circles.append(circle2)
                last_seen_circle = circle2
            else:
                new_track_circles.append(last_seen_circle)

        track = np.array(new_track_circles)[:,[0,1]].astype(float)
        new_track = [track[0,:]]
        for p2 in track[1:,:]:
            x1, y1 = new_track[-1]
            x2, y2 = p2
            dist = np.sqrt((x2 - x1)**2 + (y2 - y1)**2)
            if dist > 100:
                new_track.append(new_track[-1])
                continue        
            new_track.append(p2)

        return np.array(new_track)