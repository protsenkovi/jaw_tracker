import cv2
import numpy as np

class MillimeterSizeEstimatorByEye:
    def __init__(self, 
                 haarcascade_eye_path, 
                 avg_eye_size_mm = 12,
                 eyeDetectScaleFactor = 1.8,
                 eyeDetectMinNeighbors = 15,
                 pyrMeanShiftSp = 21,
                 pyrMeanShiftSr = 21,
                 threshold =  120,
                 thresholdType = cv2.THRESH_BINARY,
                 morphType = cv2.MORPH_DILATE,
                 morphKernelSizeX = 3,
                 morphKernelSizeY = 3,
                 gaussianBlurKernelX = 3,
                 gaussianBlurKernelY = 3,
                 gaussianBlurSigmaX = 2,
                 houghCircleDp = 1,
                 houghCircleMinDistance = 10,
                 houghCircleParam1 = 200,
                 houghCircleParam2 = 10,
                 houghCircleMinRadius = 8,
                 houghCircleMaxRadius = 30):
        self.avg_eye_size_mm = avg_eye_size_mm
        self.mean_eye_size_in_px = None
        self.mm_size_in_pixels = 1
        self.mm_size_std_in_pixels = 0
        self.mm_size_in_pixels_agg = (0,0,0)
        self.eye_count = 0
        self.eye_cascade = cv2.CascadeClassifier(haarcascade_eye_path)
        self.eyeDetectScaleFactor = eyeDetectScaleFactor
        self.eyeDetectMinNeighbors = eyeDetectMinNeighbors
        self.pyrMeanShiftSp = pyrMeanShiftSp
        self.pyrMeanShiftSr = pyrMeanShiftSr
        self.threshold = threshold
        self.thresholdType = thresholdType
        self.morphType = cv2.MORPH_DILATE
        self.morphKernelSizeX = morphKernelSizeX
        self.morphKernelSizeY = morphKernelSizeY
        self.gaussianBlurKernelX = gaussianBlurKernelX
        self.gaussianBlurKernelY = gaussianBlurKernelY
        self.gaussianBlurSigmaX = gaussianBlurSigmaX
        self.houghCircleDp = houghCircleDp
        self.houghCircleMinDistance = houghCircleMinDistance
        self.houghCircleParam1 = houghCircleParam1
        self.houghCircleParam2 = houghCircleParam2
        self.houghCircleMinRadius = houghCircleMinRadius
        self.houghCircleMaxRadius = houghCircleMaxRadius

    def __update_mean_and_std__(self, newValue):
        (count, mean, M2) = self.mm_size_in_pixels_agg
        count += 1
        delta = newValue - mean
        mean += delta / count
        delta2 = newValue - mean
        M2 += delta * delta2
        self.mm_size_in_pixels_agg = (count, mean, M2)

    def update_estimate(self, frame):
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        eyes = self.eye_cascade.detectMultiScale(gray, self.eyeDetectScaleFactor, self.eyeDetectMinNeighbors)
       
        for x,y,w,h in eyes[:2]:
            eye_image = frame[y:y+h,x:x+w]        
            eye_image = cv2.cvtColor(eye_image, cv2.COLOR_BGR2HSV)        
            eye_image = ~cv2.pyrMeanShiftFiltering(eye_image, self.pyrMeanShiftSp, self.pyrMeanShiftSr)       
            eye_image = cv2.cvtColor(cv2.cvtColor(eye_image, cv2.COLOR_HSV2BGR), cv2.COLOR_BGR2GRAY)      
            ret, eye_image = cv2.threshold(eye_image, self.threshold, 255, self.thresholdType)
            eye_image = cv2.morphologyEx(eye_image, self.morphType, (self.morphKernelSizeX, self.morphKernelSizeY))
            eye_image = cv2.GaussianBlur(eye_image, (self.gaussianBlurKernelX, self.gaussianBlurKernelY), self.gaussianBlurSigmaX)
            
            circles = cv2.HoughCircles(eye_image, 
                cv2.HOUGH_GRADIENT, 
                self.houghCircleDp, 
                self.houghCircleMinDistance,
                param1=self.houghCircleParam1,
                param2=self.houghCircleParam2, 
                minRadius=self.houghCircleMinRadius, 
                maxRadius=self.houghCircleMaxRadius)

            if not circles is None and len(circles) > 0:
                circles = circles[0,:] 
                cx, cy, cr = int(circles[0][0]), int(circles[0][1]), circles[0][2]
            
                self.__update_mean_and_std__(cr/self.avg_eye_size_mm)
                (count, mean, M2) = self.mm_size_in_pixels_agg
                (mean, variance) = (mean, M2 / count)
                self.mm_size_in_pixels = mean
                self.mm_size_std_in_pixels = variance
        

class MillimeterGrid:
    def  __init__(self, 
                  mm_size_in_pixels,
                  distanceCaptionRPadding = 170,
                  distancecCaptionBPadding = 45,
                  correctionDistCoef = 1):
        self.mm_size_in_pixels = mm_size_in_pixels
        self.mm_size_std_in_pixels = 1

        self.distanceCaptionRPadding = distanceCaptionRPadding
        self.distancecCaptionBPadding = distancecCaptionBPadding
        self.correctionDistCoef = correctionDistCoef

        self.cell_size_caption_origin = (int(20*mm_size_in_pixels), int(90*mm_size_in_pixels))
        self.cell_size_caption_font = cv2.FONT_HERSHEY_SIMPLEX
        self.cell_size_caption_font_scale = mm_size_in_pixels*0.5
        self.cell_size_caption_color = (22+100, 26+100, 94+100)
        self.cell_size_caption_thickness = 2

        self.line_color = (22, 26, 94)
        self.minor_line_thickness = 1
        self.major_line_thickness = 2
        self.major_line_period = 100
        


    def set_mm_size_in_pixels(self, mm_size_in_pixels, mm_size_std_in_pixels):
        if mm_size_in_pixels != None:
            diff = np.abs(self.mm_size_in_pixels-mm_size_in_pixels)
            if diff > np.abs(mm_size_std_in_pixels*1.3):
                self.mm_size_in_pixels = mm_size_in_pixels


    def draw(self, frame, with_caption = True):
        height, width = frame.shape[0:2]
        distance_between_lines = self.mm_size_in_pixels

        # grid_frame = cv2.resize(frame.copy(), (int(frame.shape[1]*distance_between_lines), int(frame.shape[0]*distance_between_lines)))
        frame = cv2.resize(frame, (int(frame.shape[1]*distance_between_lines), int(frame.shape[0]*distance_between_lines)))
        
        for i in range(0, frame.shape[1], 10):
            pt1 = (i, 0)
            pt2 = (i, frame.shape[0]-1)
            is_major = i%self.major_line_period==0
            line_thickness = self.major_line_thickness if is_major else self.minor_line_thickness  
            cv2.line(frame, 
                     pt1,
                     pt2, 
                     self.line_color, 
                     line_thickness)

        for j in range(0, frame.shape[0], 10):
            pt1 = (0, int(j))
            pt2 = (frame.shape[1]-1, int(j))
            is_major = j%self.major_line_period==0
            line_thickness = self.major_line_thickness if is_major else self.minor_line_thickness  
            cv2.line(frame, 
                     pt1,
                     pt2, 
                     self.line_color, 
                     line_thickness)

        if with_caption:
            cell_size_caption_text = "{} mm".format(self.major_line_period//10)
            cv2.putText(
                frame,
                cell_size_caption_text,
                self.cell_size_caption_origin,
                self.cell_size_caption_font,
                self.cell_size_caption_font_scale,
                self.line_color,
                self.cell_size_caption_thickness*2
            )
            cv2.putText(
                frame,
                cell_size_caption_text,
                self.cell_size_caption_origin,
                self.cell_size_caption_font,
                self.cell_size_caption_font_scale,
                self.cell_size_caption_color,
                self.cell_size_caption_thickness
            )
        
        return frame

    def draw_track(self, frame, track):
        if track.shape[0] > 2:
            track = track.astype(int)
            for p1, p2 in zip(track[:-1,:], track[1:,:]):
                cv2.line(frame, 
                     (int(p1[0]*self.mm_size_in_pixels), int(p1[1]*self.mm_size_in_pixels)), 
                     (int(p2[0]*self.mm_size_in_pixels), int(p2[1]*self.mm_size_in_pixels)), 
                     (0,255,0))

    def draw_distance(self, frame, pt1, pt2):
        if not pt1 is None and not pt2 is None:
            x1, y1 = pt1
            x2, y2 = pt2
            dist = np.sqrt((x2 - x1)**2 + (y2 - y1)**2)*self.correctionDistCoef
            cv2.line(frame, 
                    (int(x1), int(y1)), 
                    (int(x2), int(y2)), 
                    self.line_color, 
                    3)
            cv2.line(frame, 
                    (int(x1), int(y1)), 
                    (int(x2), int(y2)), 
                    (255,255,255), 
                    1)
            distance_caption_origin = (int((frame.shape[1]-self.distanceCaptionRPadding)),
                                       int((frame.shape[0]-self.distancecCaptionBPadding)))

            cv2.putText(
                frame,
                "{:.0f} mm".format(dist/10),
                distance_caption_origin,
                cv2.FONT_HERSHEY_COMPLEX,
                1,
                (22, 26, 94),
                5
            )
            cv2.putText(
                frame,
                "{:.0f} mm".format(dist/10),
                distance_caption_origin,
                cv2.FONT_HERSHEY_COMPLEX,
                1,
                (255,255,255),
                2
            )