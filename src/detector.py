import cv2
import numpy as np

class FaceDetector:
    def __init__(self,
                 haarcascade_frontalface_path,
                 faceDetectScaleFactor = 2,
                 faceDetectMinNeighbors = 15,
                 lowerFaceLPadding = 130,
                 lowerFaceRPadding = 350,
                 lowerFaceTPadding = 100,
                 lowerFaceBPadding = 40):
        self.face_cascade = cv2.CascadeClassifier(haarcascade_frontalface_path)
        self.lowerFaceLPadding = lowerFaceLPadding
        self.lowerFaceRPadding = lowerFaceRPadding
        self.lowerFaceTPadding = lowerFaceTPadding
        self.lowerFaceBPadding = lowerFaceBPadding
        self.faceDetectScaleFactor = faceDetectScaleFactor
        self.faceDetectMinNeighbors = faceDetectMinNeighbors

    def detect(self, frame):
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        faces = self.face_cascade.detectMultiScale(gray, self.faceDetectScaleFactor, self.faceDetectMinNeighbors)
        face_position = faces[0] if not faces is None and len(faces) > 0 else  (20, 20, frame.shape[1]-20, frame.shape[0]-20)
        x, y, w, h = face_position
        upper_face_region = (x,y+h//4,x+w,y+h//4)
        lower_face_region = (x+self.lowerFaceLPadding,
                             y+h//2+self.lowerFaceTPadding, 
                             x+w+self.lowerFaceLPadding-self.lowerFaceRPadding, 
                             y+h//2+self.lowerFaceTPadding-self.lowerFaceBPadding)
        return upper_face_region, lower_face_region

class PinDetector:
    def __init__(self, 
                 dotHueRangeL = 35,
                 dotHueRangeR = 70,
                 medianBlurKSize = 3,
                 morphKSizeX = 7,
                 morphKSizeY = 7,
                 houghCircleDp = 1,
                 houghCircleMinDistance = 2000,
                 houghCircleParam1 = 10,
                 houghCircleParam2 = 10,
                 houghCircleMinRadius = 1,
                 houghCircleMaxRadius = 10):
        self.face_mask = None
        self.hsv_image_only_hue = None
        self.blured_hsv_image_only_hue = None
        self.in_green_color_range_mask = None
        self.blured_in_green_color_range_mask = None
        self.blured_morph_in_green_color_range_mask = None
        self.hough_circles_mask = None
        self.dotHueRangeL = dotHueRangeL
        self.dotHueRangeR = dotHueRangeR
        self.medianBlurKSize = medianBlurKSize
        self.morphKSizeX = morphKSizeX
        self.morphKSizeY = morphKSizeY
        self.houghCircleDp = houghCircleDp
        self.houghCircleMinDistance = houghCircleMinDistance
        self.houghCircleParam1 = houghCircleParam1
        self.houghCircleParam2 = houghCircleParam2
        self.houghCircleMinRadius = houghCircleMinRadius
        self.houghCircleMaxRadius = houghCircleMaxRadius


    def get_green_dot_position(self, image):
        self.hsv_image_only_hue = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
        self.hsv_image_only_hue[:, :, 1] = 255
        self.hsv_image_only_hue[:, :, 2] = 255

        self.blured_hsv_image_only_hue = cv2.medianBlur(self.hsv_image_only_hue, self.medianBlurKSize)

        self.in_green_color_range_mask = cv2.inRange(
            self.blured_hsv_image_only_hue,
            np.array([self.dotHueRangeL, 255, 255]), 
            np.array([self.dotHueRangeR, 255, 255]))

        self.blured_in_green_color_range_mask = cv2.medianBlur(self.in_green_color_range_mask, self.medianBlurKSize)

        self.blured_morph_in_green_color_range_mask = cv2.medianBlur(
            cv2.morphologyEx(
                self.blured_in_green_color_range_mask, 
                cv2.MORPH_OPEN, 
                (self.morphKSizeX,self.morphKSizeY)), 
            self.medianBlurKSize
        )

        self.hough_circles_mask = np.zeros(self.blured_morph_in_green_color_range_mask.shape, 
                                           dtype=self.blured_morph_in_green_color_range_mask.dtype)
        circles = cv2.HoughCircles(
            self.blured_morph_in_green_color_range_mask, 
            cv2.HOUGH_GRADIENT, 
            self.houghCircleDp, 
            self.houghCircleMinDistance,
            param1=self.houghCircleParam1,
            param2=self.houghCircleParam2, 
            minRadius=self.houghCircleMinRadius, 
            maxRadius=self.houghCircleMaxRadius)

        circle = None    
        if not circles is None:
            circles = np.round(circles[0,:]).astype(int)
            x, y, r = circles[0]
            cv2.circle(self.hough_circles_mask, (x,y), r, 255, thickness=-1)
            circle = circles[0]
        return circle