import cv2
import yaml
import os
import numpy as np
from datetime import datetime as dt
import time
from src.detector import PinDetector, FaceDetector
from src.grid import MillimeterSizeEstimatorByEye, MillimeterGrid
from src.tracker import PinTracker

def get_video_files_list(input_path, supported_video_formats):
    files = os.listdir(input_path)
    video_files = [f for f in files if f.split('.')[-1].lower() in supported_video_formats]
    return [input_path + f for f in video_files]

config = yaml.safe_load(open("config.yaml"))
rotate_frame = config.get('frame_rotation', -1)
# video_path = get_video_files_list(config['input_folder'], config['supported_video_formats'])[0] if config['input_method'] == 'file' else 0
video_path = config['input_folder'] + 'IMG_7489.MOV'
frames_done = 0
start_time = dt.now()
face_detector = FaceDetector(config['haarcascade_frontalface_path'])
scale_estimator = MillimeterSizeEstimatorByEye(config['haarcascade_eye_path'])
pin_detector = PinDetector()
pin_tracker = PinTracker()
grid = MillimeterGrid(mm_size_in_pixels=1.0)
is_paused = False
is_quitting = False

mouse_point_1 = None
mouse_point_2 = None

def mouse_click(event, x, y, flags, param):
    global mouse_point_1, mouse_point_2, rotate_frame
    if event == cv2.EVENT_LBUTTONDOWN:
        if not mouse_point_1 is None and not mouse_point_2 is None:
            mouse_point_1 = None
            mouse_point_2 = None
        if mouse_point_1 is None:
            mouse_point_1 = (x,y)
        elif mouse_point_2 is None:
            mouse_point_2 = (x,y)
    if event == cv2.EVENT_RBUTTONDOWN:
        mouse_point_1 = None
        mouse_point_2 = None
    if event == cv2.EVENT_MBUTTONDOWN:
        rotate_frame = (rotate_frame+2)%4-1

def frame_generator(video_capture):    
    while(True):
        ret, frame = video_capture.read()        
        if not ret:
            print("\nend")
            video_capture.release()
            break
        yield frame



for idx, frame in enumerate(frame_generator(cv2.VideoCapture(video_path))):
    if is_quitting:
        break
    frames_done = frames_done + 1
    fps = frames_done/((dt.now()-start_time).total_seconds()+0.00001)
    print("\r{} {:.1f} fps".format(idx, fps), end='')

    if rotate_frame >=0:
        frame = cv2.rotate(frame, rotate_frame)
    
    upper_face_region, lower_face_region = face_detector.detect(frame)
    ux,uy,uw,uh = upper_face_region
    lx,ly,lw,lh = lower_face_region
    upper_face = frame[uy:uy+uh,ux:ux+uw]
    lower_face = frame[ly:ly+lh,lx:lx+lw]

    circle = pin_detector.get_green_dot_position(lower_face)
    pin_tracker.append_green_dot_position(idx, circle)

    if config['estimate_mm_in_pixels_method'] == 'auto':
        scale_estimator.update_estimate(upper_face)
        grid.set_mm_size_in_pixels(scale_estimator.mm_size_in_pixels, scale_estimator.mm_size_std_in_pixels)
    elif config['estimate_mm_in_pixels_method'] == 'fixed':
        grid.set_mm_size_in_pixels(config['estimate_mm_in_pixels'])
    
    while(True):        
        canvas = lower_face.copy()
        canvas = grid.draw(canvas)
        grid.draw_track(canvas, pin_tracker.get_track_without_outliers())
        grid.draw_distance(canvas, mouse_point_1, mouse_point_2)

        cv2.imshow('frame', canvas)
        cv2.setMouseCallback("frame", mouse_click)

        retval = cv2.waitKey(1)
        if retval&0xFF == ord('p'):
            is_paused = ~is_paused
        if retval&0xFF == ord('q'):
            is_quitting = True
            break
        if retval&0xFF == ord('o'):
            screenshot_file_name = video_path.split('/')[-1].split('.')[0]+ '_{}_{}'.format(idx, dt.timestamp(dt.now()))
            cv2.imwrite(config['output_folder'] + screenshot_file_name + '.png',
                        canvas)
        
        if not is_paused:
            break
        else:
            time.sleep(0.04)



cv2.destroyAllWindows()

if config.get('frame_rotation') == None:
    with open('config.yaml', 'a+') as config_file:
        config_file.write('\n{}: {}'.format('frame_rotation', rotate_frame))