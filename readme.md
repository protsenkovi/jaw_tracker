# Шаги установки

1. Установить https://docs.conda.io/en/latest/miniconda.html Python 3.7
2. Скачать и распаковать данный репозиторий https://gitlab.com/protsenkovi/jaw_tracker/-/archive/master/jaw_tracker-master.zip
3. Выполнить в консоли Anaconda
```
conda create -n jawtracker python pip
conda activate jawtracker
pip install opencv-python numpy PyYAML
```

# Запуск

1. Поместить видео файлы в input, либо подключить веб-камеру к компьютеру
2. Вызвать run.bat

# Управление

Левая кнопка мыши - установка опорной точки для замера расстояния  
Правая кнопка мыши - сброс опорных точек  
Клавиша "английская P" - пауза  
Клавиша "английская Q" - выход из программы  
Клавиша "английская O" - сохранить скриншот  

# Конфигурация

Хранится в config.yaml. Значения параметров:
- input_method выбор входного видео потока с камеры или из файла. Варианты: 'camera', 'file'.
